import { NumbersValidator } from '../../app/numbers_validator.js';
import { expect } from 'chai';

describe('isInteger positive test', () => {
    let validator;
    beforeEach(() => {
        validator = new NumbersValidator();
    });

    afterEach(() => {
        validator = null;
    })

    it('should return true when provided with an integer', () => {
        const validationResults = validator.isInteger(4);
        expect(validationResults).to.be.equal(true);
    });

    it('should throw an error when provided a string', () => {
        expect(() => {
            validator.isInteger("helo");
        }).to.throw('[helo] is not a number');
    });


});