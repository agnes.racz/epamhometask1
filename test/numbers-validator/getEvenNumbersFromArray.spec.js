import { NumbersValidator } from '../../app/numbers_validator.js';
import { expect } from 'chai';

describe('getEvenNumbers positive test', () => {
    let validator;
    beforeEach(() => {
        validator = new NumbersValidator();
    });

    afterEach(() => {
        validator = null;
    })

    it('should return array of even numbers', () => {
        const arr = [5, 7, 2, 4, 9];
        const evenNumbersArray = validator.getEvenNumbersFromArray(arr);
        expect(evenNumbersArray).to.be.eql([2, 4]);
    });

    it('should throw an error when array contains string', () => {
        const arr = [2, 3, "45"];
        expect(() => {
            validator.getEvenNumbersFromArray(arr)
        }).to.throw('[2,3,45] is not an array of "Numbers"');
    });


 });