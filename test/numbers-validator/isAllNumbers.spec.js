import { NumbersValidator } from '../../app/numbers_validator.js';
import { expect } from 'chai';

describe('isArrayAllNumbers positive test', () => {
    let validator;
    beforeEach(() => {
        validator = new NumbersValidator();
    });

    afterEach(() => {
        validator = null;
    })

    it('should return true when provided with an array of numbers', () => {
        const arr = [2, 3, 4, 5];
        const validationResults = validator.isAllNumbers(arr);
        expect(validationResults).to.be.equal(true);
    });

    it('should throw" an error when provided a string', () => {
        const arr = "hello";
        expect(() => {
            validator.isAllNumbers(arr);
        }).to.throw('[hello] is not an array');
    });
});